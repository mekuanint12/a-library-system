package api

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"testing"

	_ "github.com/lib/pq"
	db "github.com/mekuanint12/a-library-system/internal/adapters/framework/right/db/sqlc"
	"github.com/mekuanint12/a-library-system/util"
	"github.com/stretchr/testify/require"
)

type allMsg struct {
	Message string `json:"message"`
	Error   string `json:"error"`
}
type UpdateBookParam struct {
	Isbn       string    `json:"isbn"`
	BookStatus db.Status `json:"book_status"`
}
type bookData struct {
	Isbn        string `json:"isbn"`
	BookTitle   string `json:"book_title"`
	AuthorName  string `json:"author_name"`
	Description string `json:"description"`
	// only positive number
	NumOfPages sql.NullInt32 `json:"num_of_pages"`
	// only [ACTIVE, BLOCKED or PENDING]
	BookStatus  db.Status    `json:"book_status"`
	ReleaseDate sql.NullTime `json:"release_date"`
	CreatedAt   sql.NullTime `json:"created_at"`
	UpdatedAt   sql.NullTime `json:"updated_at"`
}
type isbnBook struct {
	Isbn string `json:"isbn"`
}

var nonAlphanumericRegex = regexp.MustCompile(`[^a-zA-Z0-9 ]+`)

func clearString(str string) string {
	return nonAlphanumericRegex.ReplaceAllString(str, "")
}
func AddRandomBook(t *testing.T) []byte {
	arg := db.AddBookParams{
		Isbn:        util.RandomISBN(),
		BookTitle:   util.RandomBookTitle(),
		AuthorName:  util.RandomAuthorName(),
		Description: util.RandomBookDesc(),
	}
	body, err := json.Marshal(arg)
	if err != nil {
		t.Fatal(err)
	}
	req, _ := http.NewRequest(http.MethodPost, "/addbook", bytes.NewBufferString(string(body)))
	recorder := httptest.NewRecorder()
	server := testServer()
	server.Router.ServeHTTP(recorder, req)

	allData, _ := ioutil.ReadAll(recorder.Body)
	return allData
}

func TestAddingBook(t *testing.T) {
	t.Run("Adding new book successfully", func(t *testing.T) {
		book := AddRandomBook(t)
		var msg allMsg
		json.Unmarshal(book, &msg)

		assertError(t, msg.Message, "Book Successfully Added")
	})
	t.Run("Getting Book Detail using ISBN", func(t *testing.T) {
		book := AddRandomBook(t)
		var data bookData
		json.Unmarshal(book, &data)
		arg := isbnBook{
			Isbn: data.Isbn,
		}
		body, err := json.Marshal(arg)
		if err != nil {
			t.Fatal(err)
		}
		req, _ := http.NewRequest(http.MethodGet, "/getbook", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)
		// t.Fatal(recorder.Body.String())
		var ereMsg allMsg
		messge, _ := ioutil.ReadAll(recorder.Body)
		json.Unmarshal(messge, &ereMsg)

		assertError(t, ereMsg.Message, data.BookTitle)
	})
	t.Run("Inserting Empty ISBN", func(t *testing.T) {
		arg := db.AddBookParams{
			Isbn:        "",
			BookTitle:   util.RandomBookTitle(),
			AuthorName:  util.RandomAuthorName(),
			Description: util.RandomBookDesc(),
		}
		body, err := json.Marshal(arg)
		if err != nil {
			t.Fatal(err)
		}
		req, _ := http.NewRequest(http.MethodPost, "/addbook", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)
		messge, _ := ioutil.ReadAll(recorder.Body)
		s := strings.Split(string(messge), ":")
		response := clearString(s[1])

		assertError(t, response, "Empty ISBN")
	})

	t.Run("Getting all Books Details", func(t *testing.T) {
		for i := 0; i < 20; i++ {
			AddRandomBook(t)
		}

		arg := db.ListBooksParams{
			Limit:  1,
			Offset: 1,
		}

		body, err := json.Marshal(arg)
		if err != nil {
			t.Fatal(err)
		}
		req, err := http.NewRequest(http.MethodGet, "/getallbook", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)
		var data []bookData
		books, _ := ioutil.ReadAll(recorder.Body)
		json.Unmarshal(books, &data)
		require.NoError(t, err)
		require.NotEmpty(t, books)
	})
	t.Run("Updating Book Status using ISBN", func(t *testing.T) {
		arg1 := db.AddBookParams{
			Isbn:        "978-0-545-01022-1",
			BookTitle:   util.RandomBookTitle(),
			AuthorName:  util.RandomAuthorName(),
			Description: util.RandomBookDesc(),
		}
		body, err := json.Marshal(arg1)
		if err != nil {
			t.Fatal(err)
		}
		req, _ := http.NewRequest(http.MethodPost, "/addbook", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)

		book := AddRandomBook(t)
		var msg bookData
		json.Unmarshal(book, &msg)

		arg2 := db.UpdateBookParams{
			Isbn:       "978-0-545-01022-1",
			BookStatus: util.RandomBookStatus(),
		}
		body1, err := json.Marshal(arg2)
		if err != nil {
			t.Fatal(err)
		}
		req2, _ := http.NewRequest(http.MethodPatch, "/updatebook", bytes.NewBufferString(string(body1)))
		recorder2 := httptest.NewRecorder()
		server2 := testServer()
		server2.Router.ServeHTTP(recorder2, req2)
		// t.Fatal(recorder2.Body.String())
		var ereMsg allMsg
		messge, _ := ioutil.ReadAll(recorder2.Body)
		json.Unmarshal(messge, &ereMsg)

		assertError(t, ereMsg.Message, "Book Updated")
	})
	t.Run("Deleting Book Detail using ISBN", func(t *testing.T) {
		arg1 := db.AddBookParams{
			Isbn:        "978-0-545-01022-1",
			BookTitle:   util.RandomBookTitle(),
			AuthorName:  util.RandomAuthorName(),
			Description: util.RandomBookDesc(),
		}
		body, err := json.Marshal(arg1)
		if err != nil {
			t.Fatal(err)
		}
		req, _ := http.NewRequest(http.MethodPost, "/addbook", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)

		book := AddRandomBook(t)
		var msg bookData
		json.Unmarshal(book, &msg)

		arg2 := db.UpdateBookParams{
			Isbn:       util.RandomISBN(),
			BookStatus: util.RandomBookStatus(),
		}
		body1, err := json.Marshal(arg2)
		if err != nil {
			t.Fatal(err)
		}
		req2, _ := http.NewRequest(http.MethodPatch, "/deletebook", bytes.NewBufferString(string(body1)))
		recorder2 := httptest.NewRecorder()
		server2 := testServer()
		server2.Router.ServeHTTP(recorder2, req2)
		// t.Fatal(recorder2.Body.String())
		var ereMsg allMsg
		messge, _ := ioutil.ReadAll(recorder2.Body)
		json.Unmarshal(messge, &ereMsg)

		assertError(t, ereMsg.Message, "")
	})

}

func assertError(t testing.TB, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
