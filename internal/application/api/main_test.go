package api

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
	db "github.com/mekuanint12/a-library-system/internal/adapters/framework/right/db/sqlc"
)

const (
	dbDriver = "postgres"
	dbSource = "postgresql://root@localhost:26257/library?sslmode=disable"
)

func testServer() *Server {
	con, err := sql.Open(dbDriver, dbSource)
	if err != nil {
		log.Fatal("Can not connect to the database:", err)
	}
	serve_book := db.New(con)
	server := NewServer(serve_book)
	return server
}
