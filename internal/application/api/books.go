package api

import (
	"database/sql"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	_ "github.com/lib/pq"
	db "github.com/mekuanint12/a-library-system/internal/adapters/framework/right/db/sqlc"
)

type AddBookRequest struct {
	Isbn        string        `json:"isbn"`
	BookTitle   string        `json:"book_title"`
	AuthorName  string        `json:"author_name"`
	Description string        `json:"description"`
	NumOfPages  sql.NullInt32 `json:"num_of_pages"`
	ReleaseDate sql.NullTime  `json:"release_date"`
}
type IsbnRequest struct {
	Isbn string `json:"isbn"`
}

type UpdateBookRequest struct {
	Isbn       string       `json:"isbn"`
	BookStatus string       `json:"book_status"`
	UpdatedAt  sql.NullTime `json:"updated_at"`
}

type ListBooksRequest struct {
	Limit  int32 `json:"limit"`
	Offset int32 `json:"offset"`
}

func (server *Server) addBook(ctx *gin.Context) {
	var req AddBookRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	arg := db.AddBookParams{
		Isbn:        req.Isbn,
		BookTitle:   req.BookTitle,
		AuthorName:  req.AuthorName,
		Description: req.Description,
		NumOfPages:  req.NumOfPages,
		ReleaseDate: req.ReleaseDate,
	}
	isbnErr := validation.Validate(arg.Isbn, validation.Required.Error("Empty ISBN"), validation.Length(14, 18).Error("Invalid ISBN"))
	if isbnErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": isbnErr.Error()})
		return
	}
	titleErr := validation.Validate(arg.BookTitle, validation.Required.Error("Empty Title"), validation.Length(2, 20))
	if titleErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": titleErr.Error()})
		return
	}
	authorErr := validation.Validate(arg.AuthorName, validation.Required.Error("Empty Author Name"), validation.Length(4, 25))
	if authorErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": authorErr.Error()})
		return
	}
	descErr := validation.Validate(arg.Description, validation.Required.Error("Empty Description"), validation.Length(2, 25))
	if descErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": descErr.Error()})
		return
	}
	_, e := server.Books.GetBook(ctx, arg.Isbn)
	if e != nil {

		_, err := server.Books.AddBook(ctx, arg)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
		ctx.JSON(http.StatusOK, gin.H{"message": "Book Successfully Added"})
	}
	if e == nil {

		ctx.JSON(http.StatusOK, gin.H{"message": "Book exists"})
	}
}

func (server *Server) getBook(ctx *gin.Context) {
	var req IsbnRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	arg := IsbnRequest{
		Isbn: req.Isbn,
	}

	isbnErr := validation.Validate(arg.Isbn, validation.Required.Error("Invalid ISBN"), validation.Length(14, 17).Error("Invalid ISBN"))
	if isbnErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": isbnErr.Error()})
		return
	}

	book, err := server.Books.GetBook(ctx, arg.Isbn)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "Book doesn't exist"})
		return
	}

	ctx.JSON(http.StatusBadRequest, gin.H{"message": book.BookTitle})
}

func (server *Server) getAllBooks(ctx *gin.Context) {
	// var req ListBooksRequest
	limit := ctx.Param("limit")
	offset := ctx.Param("offset")
	l, e := strconv.Atoi(limit)
	if e != nil {
		log.Fatal("Not valid limit type")
		l = 10
	}
	o, e := strconv.Atoi(offset)
	if e != nil {
		log.Fatal("Not valid offset type")
		o = 4
	}
	arg := db.ListBooksParams{
		Limit:  int32(l),
		Offset: int32(o),
	}

	limitErr := validation.Validate(arg.Limit, validation.Required, is.Int.Error("Only Number"), validation.Length(1, 2))
	if limitErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": limitErr.Error()})
		return
	}

	offsetErr := validation.Validate(arg.Offset, is.Int.Error("Only Number"), validation.Required, validation.Length(1, 2))
	if offsetErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": offsetErr.Error()})
		return
	}
	books, err := server.Books.ListBooks(ctx, arg)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "Book doesn't exist"})
		return
	}

	ctx.JSON(http.StatusBadRequest, gin.H{"message": books})
}

func (server *Server) updateBook(ctx *gin.Context) {
	var req UpdateBookRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	arg := db.UpdateBookParams{
		Isbn:       req.Isbn,
		BookStatus: req.BookStatus,
		UpdatedAt:  sql.NullTime{time.Now(), true},
	}

	isbnErr := validation.Validate(arg.Isbn, validation.Required.Error("Empty ISBN"), validation.Length(13, 17).Error("Invalid ISBN"))
	if isbnErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": isbnErr.Error()})
		return
	}

	statusErr := validation.Validate(arg.BookStatus, validation.Required.Error("Book Updated"))
	if statusErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": statusErr.Error()})
		return
	}
	_, err := server.Books.UpdateBook(ctx, arg)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "Book doesn't exist"})
		return
	}

	ctx.JSON(http.StatusBadRequest, gin.H{"message": "Book Updated"})
}

func (server *Server) deleteBook(ctx *gin.Context) {
	var req IsbnRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	arg := IsbnRequest{
		Isbn: req.Isbn,
	}

	isbnErr := validation.Validate(arg.Isbn, validation.Required.Error("Empty ISBN"), validation.Length(13, 17).Error("Invalid ISBN"))
	if isbnErr != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": isbnErr.Error()})
		return
	}

	err := server.Books.DeleteBook(ctx, arg.Isbn)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "Book doesn't exist"})
		return
	}

	ctx.JSON(http.StatusNotFound, gin.H{"message": "Book Deleted"})
}

// It starts the HTTP server for us
func (Server *Server) Start(address string) error {
	return Server.Router.Run(address)
}

func errorResponse(err error) gin.H {
	return gin.H{"error": err.Error()}
}
