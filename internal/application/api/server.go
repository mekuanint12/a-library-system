package api

import (
	"time"

	"github.com/gin-contrib/timeout"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	db "github.com/mekuanint12/a-library-system/internal/adapters/framework/right/db/sqlc"
)

type Server struct {
	Books  *db.Queries
	Router *gin.Engine
}

func NewServer(books *db.Queries) *Server {
	server := &Server{Books: books}

	router := gin.New()
	router.Use(gin.Logger(), gin.Recovery())
	router.POST("/addbook", timeout.New(timeout.WithTimeout(10*time.Second), timeout.WithHandler(server.addBook)))
	router.GET("/getbook", timeout.New(timeout.WithTimeout(10*time.Second), timeout.WithHandler(server.getBook)))
	router.GET("/getallbook/:limit/:offset", timeout.New(timeout.WithTimeout(10*time.Second), timeout.WithHandler(server.getAllBooks)))
	router.PATCH("/updatebook", timeout.New(timeout.WithTimeout(10*time.Second), timeout.WithHandler(server.updateBook)))
	router.DELETE("/deletebook", timeout.New(timeout.WithTimeout(10*time.Second), timeout.WithHandler(server.deleteBook)))
	server.Router = router
	return server
}
