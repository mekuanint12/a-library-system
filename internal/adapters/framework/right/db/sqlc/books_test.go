package db

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"github.com/mekuanint12/a-library-system/util"
	"github.com/stretchr/testify/require"
)

func addingRandomBook(t *testing.T) Book {
	arg := AddBookParams{
		Isbn:        util.RandomISBN(),
		BookTitle:   util.RandomBookTitle(),
		AuthorName:  util.RandomAuthorName(),
		Description: util.RandomBookDesc(),
	}

	book, err := testQueries.AddBook(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, book)

	require.Equal(t, arg.Isbn, book.Isbn)
	require.Equal(t, arg.BookTitle, book.BookTitle)
	require.Equal(t, arg.AuthorName, book.AuthorName)
	require.Equal(t, arg.Description, book.Description)

	require.NotZero(t, book.CreatedAt)
	require.NotZero(t, book.BookStatus)
	return book
}

func TestAddingBook(t *testing.T) {
	addingRandomBook(t)
}

func TestGettingBook(t *testing.T) {
	book1 := addingRandomBook(t)
	book2, err := testQueries.GetBook(context.Background(), book1.Isbn)
	require.NoError(t, err)
	require.NotEmpty(t, book2)

	require.Equal(t, book1.Isbn, book2.Isbn)
	require.Equal(t, book1.BookTitle, book2.BookTitle)
	require.Equal(t, book1.AuthorName, book2.AuthorName)
	require.Equal(t, book1.Description, book2.Description)
}

func TestUpdatingBook(t *testing.T) {
	book1 := addingRandomBook(t)

	arg := UpdateBookParams{
		Isbn:       book1.Isbn,
		BookStatus: util.RandomBookStatus(),
		UpdatedAt:  sql.NullTime{time.Now(), true},
	}
	book2, err := testQueries.UpdateBook(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, book2)

	require.Equal(t, book1.Isbn, book2.Isbn)
	require.Equal(t, book1.BookTitle, book2.BookTitle)
	require.Equal(t, book1.AuthorName, book2.AuthorName)
	require.Equal(t, book1.Description, book2.Description)
}

func TestDeleteBooks(t *testing.T) {
	book1 := addingRandomBook(t)
	err := testQueries.DeleteBook(context.Background(), book1.Isbn)
	require.NoError(t, err)

	book2, err := testQueries.GetBook(context.Background(), book1.Isbn)
	require.Error(t, err)
	require.EqualError(t, err, sql.ErrNoRows.Error())
	require.Empty(t, book2)
}

func TestLsitBooks(t *testing.T) {
	for i := 0; i < 10; i++ {
		addingRandomBook(t)
	}
	arg := ListBooksParams{
		Limit:  5,
		Offset: 5,
	}

	books, err := testQueries.ListBooks(context.Background(), arg)
	require.NoError(t, err)
	require.Len(t, books, 5)

	for _, book := range books {
		require.NotEmpty(t, book)
	}
}

// delete from books where created_at = '2022-08-07';
