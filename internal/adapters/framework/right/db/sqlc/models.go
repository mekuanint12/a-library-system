// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.15.0

package db

import (
	"database/sql"
	"database/sql/driver"
	"fmt"
)

type Status string

const (
	StatusACTIVE  Status = "ACTIVE"
	StatusBLOCKED Status = "BLOCKED"
	StatusPENDING Status = "PENDING"
)

func (e *Status) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = Status(s)
	case string:
		*e = Status(s)
	default:
		return fmt.Errorf("unsupported scan type for Status: %T", src)
	}
	return nil
}

type NullStatus struct {
	Status Status
	Valid  bool // Valid is true if String is not NULL
}

// Scan implements the Scanner interface.
func (ns *NullStatus) Scan(value interface{}) error {
	if value == nil {
		ns.Status, ns.Valid = "", false
		return nil
	}
	ns.Valid = true
	return ns.Status.Scan(value)
}

// Value implements the driver Valuer interface.
func (ns NullStatus) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.Status, nil
}

type Book struct {
	Isbn        string `json:"isbn"`
	BookTitle   string `json:"book_title"`
	AuthorName  string `json:"author_name"`
	Description string `json:"description"`
	// only positive number
	NumOfPages sql.NullInt32 `json:"num_of_pages"`
	// only [ACTIVE, BLOCKED or PENDING]
	BookStatus  NullStatus   `json:"book_status"`
	ReleaseDate sql.NullTime `json:"release_date"`
	CreatedAt   sql.NullTime `json:"created_at"`
	UpdatedAt   sql.NullTime `json:"updated_at"`
}
