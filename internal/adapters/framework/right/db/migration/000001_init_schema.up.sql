CREATE TYPE Status AS ENUM (
  'ACTIVE',
  'BLOCKED',
  'PENDING'
);

CREATE TABLE books (
  "isbn" VARCHAR(17) PRIMARY KEY NOT NULL,
  "book_title" VARCHAR(25) UNIQUE NOT NULL,
  "author_name" VARCHAR(25) NOT NULL,
  "description" VARCHAR(250) NOT NULL,
  "num_of_pages" INT,
  "book_status" Status DEFAULT 'ACTIVE',
  "release_date" DATE,
  "created_at" DATE DEFAULT (now()),
  "updated_at" DATE
);

CREATE INDEX ON books ("book_title");

CREATE INDEX ON books ("author_name");

COMMENT ON COLUMN books.num_of_pages IS 'only positive number';

COMMENT ON COLUMN books.book_status IS 'only [ACTIVE, BLOCKED or PENDING]';