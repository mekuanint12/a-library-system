-- name: AddBook :one
INSERT INTO books (
  isbn, 
  book_title,
  author_name,
  description,
  num_of_pages,
  release_date
) VALUES (
  $1, $2, $3, $4, $5, $6
) RETURNING *;

-- name: GetBook :one
SELECT * FROM books
WHERE isbn = $1 
LIMIT 1;

-- name: ListBooks :many
SELECT * FROM books
ORDER BY book_title 
LIMIT $1
OFFSET $2;

-- name: UpdateBook :one
UPDATE books 
SET book_status = $2, updated_at = $3
WHERE isbn = $1 
RETURNING *;

-- name: DeleteBook :exec
DELETE FROM books WHERE isbn = $1;
