package util

import (
	"math/rand"
	"strings"
	"time"
)

const allAlphabets = "abcdefghijklmnpqrstuvwxyz"
const allNums = "0123456789"

func init() {
	rand.Seed(time.Now().Unix())
}

func RandomPageNum(min, max int32) int32 {
	return min + rand.Int31n(max-min+1)
}

func RandomString(n int) string {
	var sb strings.Builder
	k := len(allAlphabets)
	for i := 0; i < n; i++ {
		c := allAlphabets[rand.Intn(k)]
		sb.WriteByte(c)
	}
	return sb.String()
}

func RandomNumber(n int) string {
	var sb strings.Builder
	k := len(allNums)
	for i := 0; i < n; i++ {
		c := allNums[rand.Intn(k)]
		sb.WriteByte(c)
	}
	return sb.String()
}

// 978-0-545-01022-1
func RandomISBN() string {
	// a := RandomNumber(3)
	b := RandomNumber(1)
	c := RandomNumber(2)
	d := RandomNumber(6)
	e := RandomNumber(1)
	f := "978" + "-" + b + "-" + c + "-" + d + "-" + e
	return f
}
func RandomAuthorName() string {
	return RandomString(6)
}

func RandomBookTitle() string {
	return RandomString(7)
}

func RandomBookDesc() string {
	return RandomString(10)
}

func RandomBookStatus() string {
	stat := []string{"ACTIVE", "BLOCKED", "PENDING"}
	n := len(stat)
	return stat[rand.Intn(n)]
}
