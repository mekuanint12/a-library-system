package main

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
	db "github.com/mekuanint12/a-library-system/internal/adapters/framework/right/db/sqlc"
	"github.com/mekuanint12/a-library-system/internal/application/api"
	"github.com/mekuanint12/a-library-system/util"
)

func main() {
	config, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("Cant load config file: ", err)
		return
	}
	con, err := sql.Open(config.DBDriver, config.DBSource)
	if err != nil {
		log.Fatal("Can not connect to the database:", err)
		return
	}
	serve_book := db.New(con)
	server := api.NewServer(serve_book)

	err = server.Start(config.ServerAddress)
	if err != nil {
		log.Fatal("Can not start server:", err)
		return
	}

}
