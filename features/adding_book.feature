@adding_books
Feature: Adding book details to the system
    This is a feature where, a librarian or a book keeper can add book details into the sysytem.

  Background: 
    Given There is a new book to add 

  @adding_books_successfully
  Scenario: Adding book details Book Successfully Added
    When A librarian creates book's detail by inserting "<isbn>" and "<book_title>" and "<author_name>"  and "<description>" and "<num_of_pages>" and "<release_date>" 
    Then The result should be "<expected result>"
    Examples: Adding a new book
    |      isbn         | book_title  |  author_name    |       description        | num_of_pages | release_date |     expected result     |
    | 978-0-545-01022-1 |  Zegora     | Yesmake Werku   | Currently the Best Novel |    245       |  2020-03-26  |  Book Successfully Added  | 
    | 978-0-545-01023-7 |  Lela Sew   | Dr. Dawit Wendu | Psycology in deepth.     |    365       |  2019-03-20  |  Book Successfully Added  |
    | 978-0-545-01094-8 |  Emegua     |  Dawit Wendu    | Psycology in deepth.     |    368       |  2019-03-20  |  Book Successfully Added  |
  
  @adding_books_with_invalid_data
  Scenario: A librarian didn't provide a valid data
    When A librarian creates book's detail by inserting "<isbn>" and "<book_title>" and "<author_name>"  and "<description>" and "<num_of_pages>" and "<release_date>" 
    Then The result should be "<expected result>"
    Examples: Validation errors
    |      isbn         | book_title  |  author_name    |       description        | num_of_pages | release_date | expected result      |
    | 978-0-545         |  Lela Sew   | Dr. Dawit Wendu | Psycology in deepth.     |    365       |  2019-03-26  |   Invalid ISBN       |
    |                   |  Lela Sew   | Dr. Dawit Wendu | Psycology in deepth.     |    365       |  2019-03-26  |    Empty ISBN      |
    | 978-0-565-01026-1 |  Melhek     |                 | Currently the Best Novel |    245       |  2020-07-27  |   Empty Author Name  | 
    | 978-0-545-01722-3 |  Zegora     |   Mola Maru     |                          |    245       |  2020-04-15  |   Empty Description  | 
    | 978-0-545-05022-3 |             |   Mola Maru     |      Best Fiction Ever   |    245       |  2020-04-15  |   Empty Title        | 

  @adding_books_without_release_date_and_numPage
  Scenario:  A librarian didn't provide the release_date
    When A librarian creates book's detail by inserting "<isbn>" and "<book_title>" and "<author_name>"  and "<description>" and "<num_of_pages>" and "<release_date>" 
    Then The result should be "<expected result>"
    Examples: Relese date and page number should be opttional
    |      isbn         | book_title  |  author_name    |       description        | num_of_pages | release_date |     expected result       |
    | 978-0-565-01522-1 |  The King   | Yesmake Werku   |    Best Novel Ever       |       456   |              |  Book Successfully Added  | 

  @adding_an_existing_book
  Scenario:  A librarian inserted an existing book
    When A librarian creates book's detail by inserting "<isbn>" and "<book_title>" and "<author_name>"  and "<description>" and "<num_of_pages>" and "<release_date>" 
    Then The result should be "<expected result>"
    Examples: adding existing book errors
    |      isbn         | book_title  |  author_name    |       description        | num_of_pages | release_date | expected result |
    | 978-0-545-01022-1 |  Zegora     | Yesmake Werku   | Currently the Best Novel |    245       |  2020-03-26  |  Book exists  | 
    