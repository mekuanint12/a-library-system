@listing_books
Feature: Listing all the book details
    This is a feature where, a librarian or a book keeper can list out a book's detail from the sysytem.

  Background: 
    Given There is a book detail in the system 

  @listing_book_by_isbn
  Scenario: Listing a book details by isbn
    When A librarian listes detail's of books by inserting books "<isbn>" 
    Then The result should be "<expected result>"
    Examples: listing book by isbn
    |      isbn         |  expected result |
    | 978-0-545-01022-1 |  Zegora          | 
    | 978-0-545-01023-7 |  Lela Sew        |

  @listing_non_existing_book
  Scenario:  A librarian inserted an non existing existing book
    When A librarian listes detail's of books by inserting books "<isbn>" 
    Then The result should be "<expected result>"
    Examples: non existing book error
    |      isbn         | expected result     |
    | 990-0-545-01022-1 |  Book doesnt exist | 