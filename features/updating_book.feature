@updating_status
Feature: Updating the book status
    This is a feature where, a librarian or a book keeper updates the status of a book

  Background: 
    Given There is a book detail in the system 

  @updating_status
  Scenario: Updating book's status
    When A librarian updates book's detail by inserting "<isbn>" 
    Then The result should be "<expected result>"
    Examples: Updating book's status
    |      isbn         |  status   | expected result |
    | 978-0-545-01022-1 |  PENDING  |  Book Updated   |
    | 978-0-545-01023-7 |  ACTIVE   |  Book Updated   | 
    | 978-0-545-01094-8 |  BLOCKED  |  Book Updated   |
  
  @updating_non_existing_book
  Scenario:  A librarian inserted an non existing existing book
    When A librarian listes detail's of books by inserting books "<isbn>" 
    Then The result should be "<expected result>"
    Examples: non existing book error
    |      isbn         |    expected result  |
    | 990-0-545-01822-3 |  Book doesnt exist  | 
