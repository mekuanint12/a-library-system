package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"

	"github.com/cucumber/godog"
	_ "github.com/lib/pq"
	db "github.com/mekuanint12/a-library-system/internal/adapters/framework/right/db/sqlc"
	"github.com/mekuanint12/a-library-system/internal/application/api"
	"github.com/mekuanint12/a-library-system/util"
)

type isbnBook struct {
	Isbn string `json:"isbn"`
}

type TestX struct {
	suite    *godog.ScenarioContext
	response string
}

func testServer() *api.Server {
	config, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("Cant load config file: ", err)
	}
	con, err := sql.Open(config.DBDriver, config.DBSource)
	if err != nil {
		log.Fatal("Can not connect to the database:", err)
	}
	create_account := db.New(con)
	server := api.NewServer(create_account)
	return server
}

var nonAlphanumericRegex = regexp.MustCompile(`[^a-zA-Z0-9 ]+`)

func clearString(str string) string {
	return nonAlphanumericRegex.ReplaceAllString(str, "")
}

func (ts *TestX) aLibrarianCreatesBooksDetailByInsertingAndAndAndAndAnd(isbn, book_title, author_name, description string, num_of_pages int, release_date string) error {
	arg := db.AddBookParams{
		Isbn:        isbn,
		BookTitle:   book_title,
		AuthorName:  author_name,
		Description: description,
		// NumOfPages:  sql.NullTime("2020-09-02", true),
		// ReleaseDate: sql.NullInt32(345, true),
	}
	body, err := json.Marshal(arg)
	if err != nil {
		log.Fatal(err)
	}
	req, _ := http.NewRequest(http.MethodPost, "/addbook", bytes.NewBufferString(string(body)))
	recorder := httptest.NewRecorder()
	server := testServer()
	server.Router.ServeHTTP(recorder, req)
	messge, _ := ioutil.ReadAll(recorder.Body)
	s := strings.Split(string(messge), ":")
	ts.response = clearString(s[1])

	return nil
}

func thereIsANewBookToAdd() error {
	return nil
}

func thereIsABookDetailInTheSystem() error {
	return nil
}

func (ts *TestX) aLibrarianListesDetailsOfBooksByInsertingBooks(isbn string) error {
	arg := isbnBook{
		Isbn: isbn,
	}
	body, err := json.Marshal(arg)
	if err != nil {
		log.Fatal(err)
	}
	req, _ := http.NewRequest(http.MethodGet, "/getbook", bytes.NewBufferString(string(body)))
	recorder := httptest.NewRecorder()
	server := testServer()
	server.Router.ServeHTTP(recorder, req)
	messge, _ := ioutil.ReadAll(recorder.Body)
	s := strings.Split(string(messge), ":")
	ts.response = clearString(s[1])
	return nil
}
func (ts *TestX) aLibrarianUpdatesBooksDetailByInserting(isbn string) error {
	arg := isbnBook{
		Isbn: isbn,
	}
	body, err := json.Marshal(arg)
	if err != nil {
		log.Fatal(err)
	}
	req, _ := http.NewRequest(http.MethodPatch, "/updatebook", bytes.NewBufferString(string(body)))
	recorder := httptest.NewRecorder()
	server := testServer()
	server.Router.ServeHTTP(recorder, req)
	messge, _ := ioutil.ReadAll(recorder.Body)
	s := strings.Split(string(messge), ":")
	ts.response = clearString(s[1])
	return nil
}

func (ts *TestX) theResultShouldBe(expected string) error {
	if ts.response != expected {
		return fmt.Errorf("expected %s, got %s", expected, ts.response)
	}
	return nil
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	db := TestX{
		suite: ctx,
	}
	ctx.Step(`^A librarian creates book\'s detail by inserting "([^"]*)" and "([^"]*)" and "([^"]*)"  and "([^"]*)" and "([^"]*)" and "([^"]*)"$`, db.aLibrarianCreatesBooksDetailByInsertingAndAndAndAndAnd)
	ctx.Step(`^The result should be "([^"]*)"$`, db.theResultShouldBe)
	ctx.Step(`^There is a new book to add$`, thereIsANewBookToAdd)

	ctx.Step(`^A librarian listes detail\'s of books by inserting books "([^"]*)"$`, db.aLibrarianListesDetailsOfBooksByInsertingBooks)
	ctx.Step(`^There is a book detail in the system$`, thereIsABookDetailInTheSystem)
	ctx.Step(`^A librarian updates book\'s detail by inserting "([^"]*)"$`, db.aLibrarianUpdatesBooksDetailByInserting)
}
